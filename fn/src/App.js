import './App.css';
import { useState , useEffect, useRef} from "react";
import ChooseLobby from "./componants/chooseLoby"
import Lobby from './componants/lobby';
import Game from './componants/game';

function App() {

  const [lobbys, setLobbys] = useState([])
  const [sockLoad, setSockLoad] = useState(false)

  const [roomSelected, setRoomSelected] = useState("")
  const [playerList, setPlayerList] = useState([])

  const [gameStarted, setGameStarted] = useState(false)
  const [playerStatus, setPlayerStatus] = useState([])

  const web = useRef(null)


  useEffect(() => {
    web.current = new WebSocket('ws://192.168.0.16:20000/ws');

    web.current.onopen = () => {
      console.log("client: connected")
    }

    web.current.onclose = (event) => {
      console.log("client: connection closed")
    }

    web.current.onmessage = (event) => {
      try {
        let msg = JSON.parse(event.data)
        console.log(msg)
        switch(msg.command) {
          case "ListSaloons":
            setLobbys(msg.data)
            break
          case "ListSaloonPlayers":
            setPlayerList(msg.data)
            break
          case "StartGame":
            setGameStarted(true)
            break
          case "PlayGetStatus":
            setPlayerStatus(msg.data)
            break
        }
      } catch {
        console.log(event.data)
      }
    }

  }, [])

  useEffect(() => {
    setSockLoad(true)
  }, [sockLoad])

  useEffect(() => {
    console.log(roomSelected)
  }, [roomSelected])

  if (gameStarted) {
    return(
      <Game sendMsg={web.current.send.bind(web.current)} playerStatus={playerStatus}></Game>
    )
  }
  else if (sockLoad && roomSelected == "")
    return (
      <ChooseLobby sendMsg={web.current.send.bind(web.current)} lobby={lobbys} selectRoom={setRoomSelected}/>
    );
  else if (roomSelected) {
    console.log(playerList)
    return (
      <Lobby sendMsg={web.current.send.bind(web.current)} id={roomSelected} playerList={playerList}/>
    )
  }
  else
      return (
        <div></div>
      )
      
}

export default App;
