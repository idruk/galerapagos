import { useState , useEffect, useRef} from "react";


function ChooseLobby(props) {

    const [name, setName] = useState("")
  
    useEffect(() => {
        setTimeout(() => {props.sendMsg("ListSaloons")}, 1000)
    }, [])

    function onSubmitName(e) {
        if (e.key == "Enter") {
            const msg = `SetUserName\t${name}`
            props.sendMsg(msg)
        }
    }

    function onClickLobby(id) {
        setTimeout(() => { props.sendMsg(`SetUserName\t${name}`)}, 10)
        setTimeout(() => { props.sendMsg(`JoinSaloon\t${id}`); props.selectRoom(id)}, 50)
    }

    useEffect(() => {
        // console.log(props.lobby)

    }, [props.lobby])


    return (
        <div>
            <input placeholder="choose name" onChange={event => {setName(event.target.value)}} onKeyUp={e => {onSubmitName(e); e.preventDefault()}}></input>
            {
                Object.entries(props.lobby).map((item, index) => {
                    return (
                        <div key={index} onClick={() => { onClickLobby(item[0]) }}> {item[0]} </div>
                    )
                })
            }
        </div>
    )
  }
  

export default ChooseLobby;