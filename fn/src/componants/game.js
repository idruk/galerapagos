import { useEffect, useState } from "react";

function Game(props) {

    const [cards, setCards] = useState([])

    useEffect(() => {
        //qsd
        console.log(props.playerStatus.cards)
    }, [props.playerStatus])

    useEffect(() => {
        props.sendMsg("PlayGetStatus")
    }, [])

    return (
        <div className="flex flex-wrap content-start">
            <div className="w-40 border">
                <div className="font-bold">Cards:</div>
                {
                    props.playerStatus.cards.map((item, index) => {
                        return(
                            <div key={"card-" + index}>{item}</div>
                        )
                    })
                }
            </div>
            <div className="w-40 border">
                <div className="font-bold">food:</div>
                    <div>{props.playerStatus.food}</div>
            </div>
            <div className="w-40 border">
                <div className="font-bold">water:</div>
                <div>{props.playerStatus.water}</div>
            </div>
            <div className="w-40 border">
                <div className="font-bold">wood:</div>
                <div>{props.playerStatus.wood}</div>

            </div>
            <div className="w-40 border">
                <div className="font-bold">boats:</div>
                <div>{props.playerStatus.boat}</div>

            </div>
        </div>
    )
}

export default Game;