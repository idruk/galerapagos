import { useState , useEffect, useRef} from "react";
// import web from './../webSocket'

const data = {
    names:
        [
            "alex",
            "alex1",
            "alex2"
        ]
}

// BoardGetCards,
// 	BoardGetFoods,
// 	BoardGetWaters,
// 	BoardGetWeather,
// 	EndGame,
// 	JoinSaloon,
// 	LeaveGame,
// 	LeaveSaloon,
// 	ListBoardPlayers,
// 	ListPlayerCards,
// 	ListSaloons,
// 	ListSaloonPlayers,
// 	PlayGetCard,
// 	PlayGetFood,
// 	PlayGetWater,
// 	PlayGetWood,
// 	PlayUseCard,
// 	SetUserName,
// 	StartGame,

function Lobby(props) {

    const maxPlayer = 12
    const [cells, setCells] = useState([])
    const [setedCells, setSetedCells] = useState(0)

    function createPlayerCells() {
        let tmp = []

        for (let i = 0; i < maxPlayer; i++) {
            let content = ""

            if (props.playerList[i]) {
                content = props.playerList[i].name
            }

            tmp.push(
                {
                    className: "border-black border w-1/2 rounded h-16 text-center text-lg",
                    content: content,
                    key: i
                }
            )
        }
        setCells(tmp)
    }

    function onStartGame() {
        props.sendMsg("StartGame")
    }

    useEffect(() => {
        const msg = `ListSaloonPlayers\t${props.id}`
        props.sendMsg(msg)
    }, [])

    useEffect( () => {
        console.log(props.playerList)
        if (props.playerList)
            createPlayerCells()
    }, [props.playerList])

    return (
        <div className="h-screen flex flex-col items-center 
              justify-center border  rounded">
            {
                cells.map((item) => {
                    return ( 
                        <div className={item.className} key={item.key}>{item.content}</div>
                    )
                })
            }
            <button onClick={onStartGame}>Start Game</button>
            {/* <input className="border-solid border"></input> */}

        </div>
    );
}

export default Lobby;